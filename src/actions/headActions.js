import {
    SET_TAGLINE
  } from "./actionType";
   
  // to set the tagline in header
  export const setTagline = tagline => {
    return {
      type: SET_TAGLINE,
      payload: {
        tagline
      }
    }
  }