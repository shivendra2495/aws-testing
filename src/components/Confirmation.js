import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import {List, ListItem} from 'material-ui/List';
import RaisedButton from 'material-ui/RadioButton';
// import { StylesContext } from '@material-ui/styles';
export class Confirmation extends Component {
   Continue = e => {
    e.preventDefault();
    this.props.nextStep();
  }
  Back = e => {
    e.preventDefault();
    this.props.prevStep();
  }
  render() {
    const  { values: { email, firstName, lastName, city, occupation, bio } } = this.props;
    return (
      <MuiThemeProvider>
        <React.Fragment><AppBar title='Confirm Details' />
        <List>
          <ListItem 
          primaryText={'First Name'}
          secondaryText={firstName}

          />
          <ListItem 
          primaryText='Last Name'
          secondaryText={lastName}
          />
          <ListItem 
          primaryText='Email'
          secondaryText={email}
          />
          <ListItem 
          primaryText='City'
          secondaryText={city}
          />
          <ListItem 
          primaryText='Occupation'
          secondaryText={occupation}
          />
          
          <ListItem 
          primaryText='Bio'
          secondaryText={bio}
          />
        </List>
 
        <br/>
        <RaisedButton
        label="Continue"
        primary={true}
        style={styles.button}
        onClick={this.Continue}
        />
         <RaisedButton
        label="Back"
        primary={false}
        style={styles.button}
        onClick={this.Back}
        />
        </React.Fragment></MuiThemeProvider>
    )
  }
}
const styles = {
  button: {
    margin: 15
  }
}
export default Confirmation;