import React, { Component } from 'react';
import UserDetails from './UserDetails';
import PersonalDetails from './PersonalDetails';
import Confirmation from './Confirmation';
import Success from './Success';


export default class Signup extends Component {
    state = {
        step: 1,
        email: '',
        username: '',
        password: '',
        firstName: '',
        lastName: '',
        city: '',
        occupation: '',
        bio: '',
    }
    // go back to previous step
    prevStep = () => {
        const { step } = this.state;
        this.setState({ step: step - 1 });
    }
    // proceed to the next step
    nextStep = () => {
        const { step } = this.state;
        this.setState({ step: step + 1 });
    }
    // handle field change
    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    }
  render() {
    const { step } = this.state;
    const { email, username, password, firstName, lastName, city, occupation, bio } = this.state;
    const values = { email, username, password, firstName, lastName, city, occupation, bio }
    switch (step) {
        case 1: 
          return (
            <UserDetails 
            nextStep=  { this.nextStep}
            handleChange= {this.handleChange}
            values= {this.state}
            />
          )
        case 2: 
          return (
            <PersonalDetails 
            nextStep=  { this.nextStep}
            prevStep=  { this.prevStep}
            handleChange= {this.handleChange}
            values= {this.state}
            />
          )
        case 3: 
          return (
            <Confirmation 
            nextStep=  { this.nextStep}
            prevStep=  { this.prevStep}
            values= {this.state}
            />          )
        case 4:
          return (
            <Success />
          )
        // never forget the default case, otherwise VS code would be mad!
        default: 
           // do nothing
      }
  }
}
